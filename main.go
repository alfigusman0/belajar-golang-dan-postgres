package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/alfigusman0/belajar-golang-dan-postgres/controllers"
	"gitlab.com/alfigusman0/belajar-golang-dan-postgres/models"
)

func main() {
	// Migration Models
	models.MigrationUsers()
	models.MigrationIdeas()

	// Router
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	// Router Controllers Ideas
	r.POST("/Ideas", controllers.CreateIdea)
	r.GET("/Ideas", controllers.ReadIdeas)
	r.PUT("/Ideas", controllers.UpdateIdea)
	r.DELETE("Ideas", controllers.DeleteIdea)
	r.GET("/Ideas/:id", controllers.WhereIdeas)
	// Router Controllers Users
	r.POST("/Users", controllers.CreateUser)
	r.GET("/Users", controllers.ReadUsers)
	r.PUT("/Users", controllers.UpdateUser)
	r.DELETE("Users/", controllers.DeleteUser)
	r.GET("/Users/:id", controllers.WhereUsers)
	r.POST("/Login", controllers.LoginUsers)

	err := r.Run()
	if err != nil {
		panic(err)
	}
}

