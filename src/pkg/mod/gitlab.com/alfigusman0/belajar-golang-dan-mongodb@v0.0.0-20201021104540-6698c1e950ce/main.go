package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/alfigusman0/belajar-golang-dan-mongodb/config"
	"gitlab.com/alfigusman0/belajar-golang-dan-mongodb/controllers"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
)

func main() {
	/* Check Connection */
	c := config.GetClient()
	err := c.Ping(context.Background(), readpref.Primary())
	if err != nil {
		log.Fatal("Couldn't connect to the database", err)
	} else {
		log.Print("Connected!")
	}

	// Router
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.GET("/welcome", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"error":   false,
			"message": "Hello bro",
		})
	})

	// Router Controllers Ideas
	r.POST("/Ideas", controllers.CreateIdea)
	r.GET("/Ideas", controllers.ReadIdeas)
	r.PUT("/Ideas", controllers.UpdateIdea)
	r.DELETE("Ideas", controllers.DeleteIdea)
	r.GET("/Ideas/:id", controllers.WhereIdeas)

	// Router Controllers Users
	r.POST("/Users", controllers.CreateUser)
	r.GET("/Users", controllers.ReadUsers)
	r.PUT("/Users", controllers.UpdateUser)
	r.DELETE("Users/", controllers.DeleteUser)
	r.GET("/Users/:id", controllers.WhereUsers)
	r.POST("/Login", controllers.LoginUsers)

	err = r.Run()
	if err != nil {
		panic(err)
	}
}
