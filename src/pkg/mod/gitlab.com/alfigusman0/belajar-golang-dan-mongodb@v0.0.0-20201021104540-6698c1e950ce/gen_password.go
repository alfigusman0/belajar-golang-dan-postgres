package main

import (
	"fmt"
	"gitlab.com/alfigusman0/belajar-golang-dan-mongodb/config"
)

func main()  {
	password := "secret"
	hash, _ := config.HashPassword(password) // ignore error for the sake of simplicity

	fmt.Println("Password:", password)
	fmt.Println("Hash:    ", hash)

	match := config.CheckPasswordHash(password, hash)
	fmt.Println("Match:   ", match)
}
