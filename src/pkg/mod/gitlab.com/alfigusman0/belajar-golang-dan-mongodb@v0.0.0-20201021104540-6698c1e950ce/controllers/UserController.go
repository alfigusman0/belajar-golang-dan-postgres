package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/alfigusman0/belajar-golang-dan-mongodb/config"
	"gitlab.com/alfigusman0/belajar-golang-dan-mongodb/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
)

func CreateUser(c *gin.Context) {
	db := config.GetClient()
	email := c.PostForm("email")
	where := bson.M{"email": email}
	user := models.WhereUsers(db, where)
	if user.Email == email {
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Email sudah terdaftar.",
		})
	}else{
		password := c.PostForm("password")
		confirmPassword := c.PostForm("confirm_password")
		if password == confirmPassword {
			hash, _ := config.HashPassword(password)
			user := models.Users{
				ID: primitive.NewObjectID(),
				Name: c.PostForm("name"),
				Email: email,
				Password: hash,
			}
			insertedID := models.CreateUser(db, user)
			c.JSON(200, gin.H{
				"error":   false,
				"message": "Data berhasil ditambah.",
				"id": insertedID,
			})
		}else{
			c.JSON(200, gin.H{
				"error":   true,
				"message": "Password tidak sama.",
			})
		}
	}
}

func ReadUsers(c *gin.Context) {
	db := config.GetClient()
	filter := bson.M{}
	users := models.ReadUsers(db, filter)
	c.JSON(200, gin.H{
		"error":   false,
		"message": nil,
		"data":    users,
	})
}

func UpdateUser(c *gin.Context) {
	db := config.GetClient()
	// Declare a primitive ObjectID from a hexadecimal string
	idPrimitive, err := primitive.ObjectIDFromHex(c.PostForm("id"))
	if err != nil {
		log.Fatal("primitive.ObjectIDFromHex ERROR:", err)
	} else {
		update := bson.M{
			"title":   c.PostForm("title"),
			"details": c.PostForm("details"),
			"user":    c.PostForm("user"),
		}
		where := bson.M{"_id": idPrimitive}
		user := models.UpdateUser(db, update, where)
		if user > 0 {
			c.JSON(200, gin.H{
				"error":   false,
				"message": "User has been updated.",
			})
		} else {
			c.JSON(200, gin.H{
				"error":   true,
				"message": "User is not found.",
			})
		}
	}
}

func DeleteUser(c *gin.Context) {
	db := config.GetClient()
	// Declare a primitive ObjectID from a hexadecimal string
	idPrimitive, err := primitive.ObjectIDFromHex(c.PostForm("id"))
	if err != nil {
		log.Fatal("primitive.ObjectIDFromHex ERROR:", err)
	} else {
		where := bson.M{"_id": idPrimitive}
		user := models.DeleteUser(db, where)
		if user > 0 {
			c.JSON(200, gin.H{
				"error":   false,
				"message": "User has been removed.",
			})
		} else {
			c.JSON(200, gin.H{
				"error":   true,
				"message": "User is not found.",
			})
		}
	}
}

func WhereUsers(c *gin.Context) {
	db := config.GetClient()
	idPrimitive, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		log.Fatal("primitive.ObjectIDFromHex ERROR:", err)
	} else {
		where := bson.M{"_id": idPrimitive}
		user := models.WhereUsers(db, where)
		c.JSON(200, gin.H{
			"error":   false,
			"message": nil,
			"data":    user,
		})
	}
}

func LoginUsers(c *gin.Context) {
	db := config.GetClient()
	email := c.PostForm("email")
	where := bson.M{"email": email}
	user := models.WhereUsers(db, where)
	if user.Email == email {
		password := c.PostForm("password")
		match := config.CheckPasswordHash(password, user.Password)
		if match {
			c.JSON(200, gin.H{
				"error":   false,
				"message": nil,
				"data": user,
			})
		}else{
			c.JSON(200, gin.H{
				"error":   true,
				"message": "Password salah.",
			})
		}
	}else{
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Email tidak terdaftar.",
		})
	}
}
