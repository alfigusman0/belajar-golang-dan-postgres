package models

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"time"
)

type Ideas struct {
	ID      primitive.ObjectID `bson:"_id" json:"id"`
	Title   string             `bson:"title" json:"title"`
	Details string             `bson:"details" json:"details"`
	User    string             `bson:"user" json:"user"`
	Date    time.Time
}

func CreateIdea(client *mongo.Client, idea Ideas) interface{} {
	collection := client.Database("vidjot-dev").Collection("ideas")
	insertResult, err := collection.InsertOne(context.TODO(), idea)
	if err != nil {
		log.Fatalln("Error on inserting new Idea", err)
	}
	return insertResult.InsertedID
}

func ReadIdeas(client *mongo.Client, filter bson.M) []*Ideas {
	var ideas []*Ideas
	collection := client.Database("vidjot-dev").Collection("ideas")
	cur, err := collection.Find(context.TODO(), filter)
	if err != nil {
		log.Fatal("Error on Finding all the documents", err)
	}
	for cur.Next(context.TODO()) {
		var idea Ideas
		err = cur.Decode(&idea)
		if err != nil {
			log.Fatal("Error on Decoding the document", err)
		}
		ideas = append(ideas, &idea)
	}
	return ideas
}

func UpdateIdea(client *mongo.Client, updatedData bson.M, filter bson.M) int64 {
	collection := client.Database("vidjot-dev").Collection("ideas")
	atualizacao := bson.D{{Key: "$set", Value: updatedData}}
	updatedResult, err := collection.UpdateOne(context.TODO(), filter, atualizacao)
	if err != nil {
		log.Fatal("Error on updating one Idea", err)
	}
	return updatedResult.ModifiedCount
}

func DeleteIdea(client *mongo.Client, filter bson.M) int64 {
	collection := client.Database("vidjot-dev").Collection("ideas")
	deleteResult, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Fatal("Error on deleting one Idea", err)
	}
	return deleteResult.DeletedCount
}

func WhereIdeas(client *mongo.Client, filter bson.M) Ideas {
	var idea Ideas
	collection := client.Database("vidjot-dev").Collection("ideas")
	documentReturned := collection.FindOne(context.TODO(), filter)
	documentReturned.Decode(&idea)
	return idea
}
