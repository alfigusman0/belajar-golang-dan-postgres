package models

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"time"
)

type Users struct {
	ID       primitive.ObjectID `bson:"_id" json:"id"`
	Name     string             `bson:"name" json:"name"`
	Email    string             `bson:"email" json:"email"`
	Password string             `bson:"password" json:"password"`
	Date     time.Time
}

func CreateUser(client *mongo.Client, user Users) interface{} {
	collection := client.Database("vidjot-dev").Collection("users")
	insertResult, err := collection.InsertOne(context.TODO(), user)
	if err != nil {
		log.Fatalln("Error on inserting new User", err)
	}
	return insertResult.InsertedID
}

func ReadUsers(client *mongo.Client, filter bson.M) []*Users {
	var users []*Users
	collection := client.Database("vidjot-dev").Collection("users")
	cur, err := collection.Find(context.TODO(), filter)
	if err != nil {
		log.Fatal("Error on Finding all the documents", err)
	}
	for cur.Next(context.TODO()) {
		var user Users
		err = cur.Decode(&user)
		if err != nil {
			log.Fatal("Error on Decoding the document", err)
		}
		users = append(users, &user)
	}
	return users
}

func UpdateUser(client *mongo.Client, updatedData bson.M, filter bson.M) int64 {
	collection := client.Database("vidjot-dev").Collection("users")
	atualizacao := bson.D{{Key: "$set", Value: updatedData}}
	updatedResult, err := collection.UpdateOne(context.TODO(), filter, atualizacao)
	if err != nil {
		log.Fatal("Error on updating one User", err)
	}
	return updatedResult.ModifiedCount
}

func DeleteUser(client *mongo.Client, filter bson.M) int64 {
	collection := client.Database("vidjot-dev").Collection("users")
	deleteResult, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Fatal("Error on deleting one User", err)
	}
	return deleteResult.DeletedCount
}

func WhereUsers(client *mongo.Client, filter bson.M) Users {
	var user Users
	collection := client.Database("vidjot-dev").Collection("users")
	documentReturned := collection.FindOne(context.TODO(), filter)
	documentReturned.Decode(&user)
	return user
}
