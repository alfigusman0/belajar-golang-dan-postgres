# Belajar Golang dan MongoDB

## Go Module Init
> go mod init gitlab.com/alfigusman0/belajar-golang-dan-mongodb

## Gin Gonic
https://github.com/gin-gonic/gin
> go get -u github.com/gin-gonic/gin

## Gorm
https://github.com/go-gorm/gorm
> go get -u github.com/go-gorm/gorm

## MongoDB
https://github.com/mongodb/mongo-go-driver
> go get github.com/mongodb/mongo-go-driver/mongo

