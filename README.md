# Belajar Golang dan Postgres

## Go Module Init
> go mod init gitlab.com/alfigusman0/belajar-golang-dan-mongodb

## Create directory src on project
> mkdir src

## Set GOPATH
> export GOPATH=/project/src/  
> echo $GOPATH

## Gin Gonic
https://github.com/gin-gonic/gin
> go get -u github.com/gin-gonic/gin

## Gorm
https://gorm.io/docs/
https://github.com/go-gorm/gorm
> go get -u github.com/go-gorm/gorm

## pgx - PostgreSQL Driver and Toolkit
https://github.com/jackc/pgx
> go get -u github.com/jackc/pgx
