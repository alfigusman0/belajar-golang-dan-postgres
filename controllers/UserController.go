package controllers

import (
	"github.com/badoux/checkmail"
	"github.com/gin-gonic/gin"
	"gitlab.com/alfigusman0/belajar-golang-dan-postgres/config"
	"gitlab.com/alfigusman0/belajar-golang-dan-postgres/models"
	"html"
	"strconv"
	"strings"
)

func CreateUser(c *gin.Context) {
	email := c.PostForm("email")
	err := checkmail.ValidateFormat(email)
	if err != nil {
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Format email tidak sesuai.",
		})
	}else{
		password := c.PostForm("password")
		confirmPassword := c.PostForm("confirm_password")
		if password == confirmPassword {
			hash, _ := config.HashPassword(password)
			user := models.Users{
				Name: html.EscapeString(strings.TrimSpace(c.PostForm("name"))),
				Email: html.EscapeString(strings.TrimSpace(c.PostForm("email"))),
				Password: hash,
			}
			result := models.CreateUser(user)
			if result == 0 {
				c.JSON(200, gin.H{
					"error":   true,
					"message": "Gagal menambah user.",
				})
			}else{
				c.JSON(200, gin.H{
					"error":   false,
					"message": "Berhasil menambah user.",
				})
			}
		}else{
			c.JSON(200, gin.H{
				"error":   true,
				"message": "Password tidak sama.",
			})
		}
	}
}

func ReadUsers(c *gin.Context) {
	users := models.ReadUsers()
	c.JSON(200, gin.H{
		"error":   false,
		"message": nil,
		"data":    users,
	})
}

func UpdateUser(c *gin.Context) {
	id, _ := strconv.ParseUint(c.PostForm("id"), 10, 32)
	user := models.Users{
		Name: html.EscapeString(strings.TrimSpace(c.PostForm("name"))),
		Email: html.EscapeString(strings.TrimSpace(c.PostForm("email"))),
	}
	result := models.UpdateUser(id, user)
	if result != 1 {
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Update user gagal.",
		})
	}else{
		c.JSON(200, gin.H{
			"error":   false,
			"message": "Update user berhasil.",
		})
	}
}

func DeleteUser(c *gin.Context) {
	id, _ := strconv.ParseUint(c.PostForm("id"), 10, 32)
	result := models.DeleteUser(id)
	if result != 1 {
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Delete user gagal.",
		})
	}else{
		c.JSON(200, gin.H{
			"error":   false,
			"message": "Delete user berhasil.",
		})
	}
}

func WhereUsers(c *gin.Context) {
	id, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	users := models.WhereUserById(id)
	if users.ID == 0 {
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Data tidak tersedia",
		})
	}else{
		c.JSON(200, gin.H{
			"error":   false,
			"message": nil,
			"data":    users,
		})
	}
}

func LoginUsers(c *gin.Context) {
	email := c.PostForm("email")
	user := models.Login(email)
	if email != user.Email {
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Email tidak tersedia.",
		})
	}else{
		password := c.PostForm("password")
		match := config.CheckPasswordHash(password, user.Password)
		if match {
			c.JSON(200, gin.H{
				"error":   false,
				"message": nil,
				"data": user,
			})
		}else{
			c.JSON(200, gin.H{
				"error":   true,
				"message": "Password salah.",
			})
		}
	}
}
