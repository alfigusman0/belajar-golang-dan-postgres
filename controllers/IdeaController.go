package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/alfigusman0/belajar-golang-dan-postgres/models"
	"strconv"
)

func CreateIdea(c *gin.Context) {
	userid, _ := strconv.ParseUint(c.PostForm("user_id"), 10, 32)
	idea := models.Ideas{
		Title: c.PostForm("title"),
		Details: c.PostForm("details"),
		UserId: userid,
	}
	result := models.CreateIdea(idea)
	if result == 0 {
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Gagal menambah Idea.",
		})
	}else{
		c.JSON(200, gin.H{
			"error":   false,
			"message": "Berhasil menambah Idea.",
		})
	}

}

func ReadIdeas(c *gin.Context) {
	ideas := models.ReadIdeas()
	c.JSON(200, gin.H{
		"error":   false,
		"message": nil,
		"data":    ideas,
	})

}

func UpdateIdea(c *gin.Context) {
	id, _ := strconv.ParseUint(c.PostForm("id"), 10, 32)
	userid, _ := strconv.ParseUint(c.PostForm("user_id"), 10, 32)
	idea := models.Ideas{
		Title: c.PostForm("title"),
		Details: c.PostForm("details"),
		UserId: userid,
	}
	result := models.UpdateIdea(id, idea)
	if result != 1 {
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Update idea gagal.",
		})
	}else{
		c.JSON(200, gin.H{
			"error":   false,
			"message": "Update idea berhasil.",
		})
	}
}

func DeleteIdea(c *gin.Context) {
	id, _ := strconv.ParseUint(c.PostForm("id"), 10, 32)
	result := models.DeleteIdea(id)
	if result != 1 {
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Delete idea gagal.",
		})
	}else{
		c.JSON(200, gin.H{
			"error":   false,
			"message": "Delete idea berhasil.",
		})
	}
}

func WhereIdeas(c *gin.Context) {
	id, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	ideas := models.WhereIdeaById(id)
	if ideas.ID == 0 {
		c.JSON(200, gin.H{
			"error":   true,
			"message": "Data tidak tersedia",
		})
	}else{
		c.JSON(200, gin.H{
			"error":   false,
			"message": nil,
			"data":    ideas,
		})
	}
}
