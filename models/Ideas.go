package models

import (
	"gitlab.com/alfigusman0/belajar-golang-dan-postgres/config"
	"gorm.io/gorm"
	"log"
)

type Ideas struct {
	gorm.Model
	Title		string		`gorm:"size:255;not null;" json:"title"`
	Details		string		`gorm:"type:string;not null;" json:"details"`
	UserId		uint64		`gorm:"not null;" json:"user_id"`
	Users		Users		`gorm:"foreignKey:UserId;references:ID;constraint:OnUpdate:CASCADE,OnDelete:RESTRICT;"`
}

func MigrationIdeas()  {
	db := config.ConnectionDB()
	db.AutoMigrate(&Ideas{})
}

func CreateIdea(idea Ideas) int64 {
	db := config.ConnectionDB()
	result := db.Create(&idea) // pass pointer of data to Create
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return result.RowsAffected
}

func ReadIdeas() *[]Ideas {
	db := config.ConnectionDB()
	var ideas []Ideas
	result := db.Find(&ideas)
	db.Preload("Users").Find(&ideas)
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return &ideas
}

func UpdateIdea(id uint64, idea Ideas) int64 {
	db := config.ConnectionDB()
	result := db.Model(&Ideas{}).Where("id = ?", id).Updates(idea)
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return result.RowsAffected
}

func DeleteIdea(id uint64) int64 {
	db := config.ConnectionDB()
	result := db.Delete(&Ideas{}, id)
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return result.RowsAffected
}

func WhereIdeaById(id uint64) *Ideas {
	db := config.ConnectionDB()
	idea := Ideas{}
	result := db.First(&idea, id)
	db.Preload("Users").Find(&idea)
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return &idea
}