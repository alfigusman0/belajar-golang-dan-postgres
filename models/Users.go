package models

import (
	"gitlab.com/alfigusman0/belajar-golang-dan-postgres/config"
	"gorm.io/gorm"
	"log"
)

type Users struct {
	gorm.Model
	Name		string		`gorm:"size:255;not null;" json:"name"`
	Email		string		`gorm:"size:255;not null;unique" json:"email"`
	Password	string		`gorm:"size:255;not null;" json:"password"`
}

func MigrationUsers()  {
	db := config.ConnectionDB()
	db.AutoMigrate(&Users{})
}

func CreateUser(user Users) int64 {
	db := config.ConnectionDB()
	result := db.Create(&user) // pass pointer of data to Create
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return result.RowsAffected
}

func ReadUsers() *[]Users {
	db := config.ConnectionDB()
	users := []Users{}
	result := db.Find(&users)
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return &users
}

func UpdateUser(id uint64, user Users) int64 {
	db := config.ConnectionDB()
	result := db.Model(&Users{}).Where("id = ?", id).Updates(user)
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return result.RowsAffected
}

func DeleteUser(id uint64) int64 {
	db := config.ConnectionDB()
	result := db.Delete(&Users{}, id)
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return result.RowsAffected
}

func WhereUserById(id uint64) *Users {
	db := config.ConnectionDB()
	users := Users{}
	result := db.First(&users, id)
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return &users
}

func Login(email string) *Users {
	db := config.ConnectionDB()
	users := Users{}
	result := db.Where("email", email).First(&users)
	if result.Error != nil {
		log.Println("Error : ", result.Error)
	}
	return &users
}