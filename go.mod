module gitlab.com/alfigusman0/belajar-golang-dan-postgres

go 1.13

require (
	github.com/badoux/checkmail v1.2.1
	github.com/gin-gonic/gin v1.6.3
	github.com/jackc/pgx v3.6.2+incompatible // indirect
	github.com/ugorji/go v1.1.13 // indirect
	gitlab.com/alfigusman0/belajar-golang-dan-mongodb v0.0.0-20201021104540-6698c1e950ce
	go.mongodb.org/mongo-driver v1.4.2
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/sys v0.0.0-20201024232916-9f70ab9862d5 // indirect
	gorm.io/driver/postgres v1.0.5
	gorm.io/gorm v1.20.5
)
